# NDD Docker Sphinx

## Version 7.1.2-1

- feat: update Sphinx version to 7.1.2 and plugins versions
- BREAKING CHANGE: remove unmaintained plugin `sphinxcontrib-actdiag`
- BREAKING CHANGE: remove unmaintained plugin `sphinxcontrib-blockdiag`
- BREAKING CHANGE: remove unmaintained plugin `sphinxcontrib-nwdiag`
- BREAKING CHANGE: remove unmaintained plugin `sphinxcontrib-seqdiag`

## Version 4.1.2-2

- feat: add custom resources (images, scripts and stylesheets)
- feat: add extlinks extension
- bug: image version in the README

## Version 4.1.2-1

- feat: update Sphinx version to 4.1.2 and plugins versions

## Version 3.5.4-1

- feat: update Sphinx version to 3.5.4 and plugins versions
- fix: container name when building PDF is now different
- feat: add sphinxemoji extension

## Version 3.4.3-1

- refactor!: very large refactoring, mostly of internal code
- refactor: move source files to "src" folder
- feat: update Sphinx version to 3.4.3 and plugins versions
- test: remove Git tests since they are already done by sphinxcontrib-git-context
- test: fix failing tests because the year has changed

## Version 3.2.1-1

- Add `sphinxcontrib-mermaid` extension
- Update Sphinx version to 3.2.1 and plugins versions

## Version 2.4.4-5

- Add `sphinx-copybutton` extension David DIDIER  2020-07-09 15:59

## Version 2.4.4-4

- Update plugins versions, especially `sphinx_rtd_theme` to 0.5.0
- Set the commit user to `ddidier/sphinx-doc` during the initialization phase

## Version 2.4.4-3

- Fix README by adding a Docker image version to the command line examples
- Generate a `.gitignore` file during the initialization phase
- Fix Git tag not taken into account during the build process
- Fix Git tag not set correctly during the initialization phase

## Version 2.4.4-2

- Add debug informations on commit
- Fix commit and hash data inside the image

## Version 2.4.4-1

- Add Git repository creation at initialization
- Remove plugins as they aren't maintained anymore: googleanalytics, googlechart, googlemaps
- Update plugins versions and fix PlantUML URL
- Extract the 'git-context' pseudo-extension to an actual extension
- Fix tests failing on CI
- Update the README
- Update Sphinx version to 2.4.4 and plugins versions
- Add ignore files list to make-livehtml script
- Rewrite tests scripts and utility scripts
- Fix tests
- Add default container name to make-livehtml script
- Add image versioning and fix minor issues
- Fix dependencies
- Remove image ID in initialization script as it was wrong
- Add default container name to make-livehtml script
- Add --help to make-html script
- Add rename-images script
- Add make-pdf script
- Remove the README push from script since the API doesn't allow it anymore

## Version 2.3.1-1

- Update Sphinx version to 2.3.1 and plugins versions
- Clean up and fix tests

## Version 2.2.1-1

- Update Sphinx version to 2.2.1 and plugins versions
- Update release instructions in README

## Version 1.8.5-5

- Use the shared Docker entrypoint script

## Version 1.8.5-4

- Add testing stage to the CI pipeline

## Version 1.8.5-3

- Use shell scripts for CI
- Transfer to GitLab
- Add `interface` option to utility scripts
- Add `name` option to utility scripts

## Version 1.8.5-2

- Add development section to the README
- Add references of the Docker image having generated the project to the configuration
- Remove version from utility scripts
- Fix summary in README to be compatible with Docker Hub

## Version 1.8.5-1

- Update Sphinx version to 1.8.5 and plugins versions
- Add utility scripts
- Enable LiveReload on multiple projects at the same time
- Move initialization configuration file to its own directory
- Add test for project initialization

## Version 1.8.4-2

- Remove the `doc-git` volume
- Enhance documentation

## Version 1.8.4-1

- Update Sphinx version to 1.8.4
- Update Python version to 3.6.8

## Version 1.8.3-2

- Fix [#4 Don't fail when the user already exists](https://bitbucket.org/ndd-docker/ndd-docker-sphinx/issues/4)
- Fix failing HTML test

## Version 1.8.3-1

- Update Sphinx version to 1.8.3 and plugins versions
- Update PlantUML version to 1.2019.0

## Version 1.8.1-5

- Add French language support for PDF

## Version 1.8.1-4

- Add GIT pseudo extension for hash, tag and date

## Version 1.8.1-3

- Update plugin `sphinx_rtd_theme` version to 0.4.2, restoring the search feature

## Version 1.8.1-2

- Fix `sphinx-init` script to use Python 3.6
- Update documentation: initialisation

## Version 1.8.1-1

- New versioning scheme `<SPHINX_VERSION>-<DOCKER_IMAGE_VERSION>`
- Update Sphinx version to 1.8.1 and plugins versions
- Update Python version to 3.6.6
- Update documentation: how to install a new extension
- Add pluggins:
  - `sphinxcontrib-excel-table`
  - `sphinxcontrib-fulltoc`
  - `guzzle_sphinx_theme`
- Remove pluggins:
  - `sphinxcontrib-exceltable`
  - `sphinxcontrib-libreoffice`
- Refactor "testing framework"

## Version 0.9.0

- Fix documentation with the new entrypoint
- Fix expected test results
- Fix entrypoint
- Update Sphinx version to 1.5.5 and plugins versions

## Version 0.8.1

- Rename DOC_DIR to DATA_DIR
- Update PlantUML version
- Fix image name in Dockerfile

## Version 0.8.0

- Update Sphinx version to 1.4.6 and plugins versions
- Add LaTeX support
- Reorganize tests to support both HTML and PDF

## Version 0.7.0

- Rename Docker image to `ddidier/sphinx-doc`
- Update Sphinx version to 1.3.6
- Update plugins versions
- Add `sphinxcontrib-exceltable` back

## Version 0.6.0

- Add basic Markdow support
- Update Sphinx and plugins versions
- Remove `sphinxcontrib-exceltable` because of an incompatibility issue

## Version 0.5.0

- Add pluggins:
  - `sphinx.ext.graphviz`
  - `sphinxcontrib-actdiag`
  - `sphinxcontrib-blockdiag`
  - `sphinxcontrib-nwdiag`
  - `sphinxcontrib-seqdiag`
  - `sphinxcontrib-exceltable`
  - `sphinxcontrib-googleanalytics`
  - `sphinxcontrib-googlechart`
  - `sphinxcontrib-googlemaps`
  - `sphinxcontrib-libreoffice`
  - `sphinxcontrib-plantuml`
- Add test documentation

## Version 0.4.1

- Fix the live HTML build

## Version 0.4.0

- Rebase upon the official [python:2.7](https://hub.docker.com/_/python/) image
- Fix files permissions by using the host group of the documentation directory

## Version 0.3.4

- Add the _unselectable prompt_ Sphinx directive ([sphinx-prompt](https://github.com/sbrunner/sphinx-prompt))

## Version 0.3.3

- Fix `livehtml` target by specifying host address

## Version 0.3.2

- Fix file permissions

## Version 0.3.1

- Fix missing `acl` package
- Fix configuration file location
- Fix `sphinx-autobuild` configuration
- Add parameters passing to `sphinx-init`

## Version 0.3.0

- Rebase image upon [envygeeks/ubuntu](https://github.com/envygeeks/docker-ubuntu)
- Add reStructuredText to PDF converter ([rst2pdf](https://github.com/rst2pdf/rst2pdf))

## Version 0.2.0

- Add Sphinx documentation watcher ([sphinx-autobuild](https://github.com/GaretJax/sphinx-autobuild))
- Remove Latex and PDF support (too big)

## Version 0.1.0

- Initial commit
- Add Sphinx documentation builder ([sphinx-doc](http://sphinx-doc.org))
- Add Sphinx documentation themes ([sphinx-themes](http://docs.writethedocs.org/tools/sphinx-themes))
