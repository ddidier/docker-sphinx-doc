# ==============================================================================
# ddidier/sphinx-doc
#
# Enhance Sphinx documentation builder (https://www.sphinx-doc.org/).
# ==============================================================================

FROM       python:3.9.18-slim-bullseye
MAINTAINER David DIDIER

RUN export DEBIAN_FRONTEND=noninteractive \
 && apt-get update \
 && apt-get install -y --no-install-recommends \
        curl git gosu make sudo \
 \
 && echo "---------------------------------------- Plugins & Extensions -----" \
 \
 && pip install --no-cache-dir --upgrade 'pip == 23.3.1' \
 && pip install --no-cache-dir \
        'Sphinx                        == 7.1.2'     \
        'guzzle_sphinx_theme           == 0.7.11'    \
        'livereload                    == 2.6.3'     \
        'recommonmark                  == 0.7.1'     \
        'rinohtype                     == 0.5.4'     \
        'sphinx-autobuild              == 2021.3.14' \
        'sphinx_bootstrap_theme        == 0.8.1'     \
        'sphinx-copybutton             == 0.5.2'     \
        'sphinx-prompt                 == 1.7.0'     \
        'sphinx-rtd-theme              == 1.3.0'     \
        'sphinxcontrib-excel-table     == 1.0.8'     \
        'sphinxcontrib-fulltoc         == 1.2.0'     \
        'sphinxcontrib-git-context     == 0.1.0'     \
        'sphinxcontrib-mermaid         == 0.9.2'     \
        'sphinxcontrib-plantuml        == 0.27'      \
        'sphinxemoji                   == 0.2.0'     \
 && pip list --outdated   \
 \
 && echo "---------------------------------------------------- PlantUML -----" \
 \
 && apt-get install -y --no-install-recommends \
        dvipng graphviz \
        openjdk-11-jre-headless \
 \
 && PLANTUML_VERSION=1.2023.7 \
 && mkdir /opt/plantuml \
 && curl -L "https://sourceforge.net/projects/plantuml/files/${PLANTUML_VERSION}/plantuml.${PLANTUML_VERSION}.jar/download" --output "/opt/plantuml/plantuml.${PLANTUML_VERSION}.jar" \
 && ln -s "/opt/plantuml/plantuml.${PLANTUML_VERSION}.jar" "/opt/plantuml/plantuml.jar" \
 \
 && echo "--------------------------------------------------- Latex PDF -----" \
 \
 && apt-get install -y --no-install-recommends \
        latexmk texlive-fonts-recommended texlive-lang-french texlive-latex-extra texlive-latex-recommended tex-gyre \
 \
 && echo "---------------------------------------------------- Cleaning -----" \
 \
 && apt-get autoremove -y \
 && rm -rf /var/cache/* \
 && rm -rf /var/lib/apt/lists/*

ARG GIT_COMMIT_SHA
ARG GIT_COMMIT_TAG

COPY files/opt/ddidier/  /opt/ddidier/
COPY files/usr/local/    /usr/local/
COPY files/usr/share/    /usr/share/

RUN chown root:root  /usr/local/bin/* \
 && chmod 755        /usr/local/bin/*

ENV DATA_DIR=/data \
    GIT_COMMIT_SHA=$GIT_COMMIT_SHA \
    GIT_COMMIT_TAG=$GIT_COMMIT_TAG

WORKDIR $DATA_DIR

ENTRYPOINT ["/usr/local/bin/docker-entrypoint"]

CMD ["bash"]
