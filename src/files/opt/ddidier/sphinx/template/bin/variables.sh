#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# This file is sourced by the scripts in the same directory.
# ------------------------------------------------------------------------------

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"



# The name of the project
# shellcheck disable=SC2155
export DDIDIER_SPHINX_PROJECT_NAME="$(basename "${PROJECT_DIR}")"

# The files Sphinx Live HTML must ignore
export DDIDIER_SPHINX_PROJECT_IGNORED_FILES=(
    "*.bak"
    "*.tmp"
)



# The version of the Docker image ddidier/sphinx-doc
export DDIDIER_SPHINX_IMAGE_VERSION="not-set"

# The name of the container Sphinx is running in
export DDIDIER_SPHINX_CONTAINER_NAME="${DDIDIER_SPHINX_PROJECT_NAME}"

# The network interface on the host machine the container is bound to
export DDIDIER_SPHINX_CONTAINER_EXPOSED_INTERFACE=127.0.0.1

# The port on the host machine the container is listening to
export DDIDIER_SPHINX_CONTAINER_EXPOSED_PORT=8000
