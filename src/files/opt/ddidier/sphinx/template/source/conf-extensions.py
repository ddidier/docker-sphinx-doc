

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom ones.
extensions = [
    'recommonmark',

    'sphinx.ext.extlinks',
    'sphinx.ext.graphviz',
    'sphinx.ext.ifconfig',
  # 'sphinx.ext.imgmath',
    'sphinx.ext.mathjax',
    'sphinx.ext.todo',

    'sphinx_copybutton',
    'sphinx-prompt',
    'sphinxemoji.sphinxemoji',

    'sphinxcontrib.excel_table',
    'sphinxcontrib.git_context',
  # 'sphinxcontrib.mermaid',
    'sphinxcontrib.plantuml',
]

# If true, 'todo' and 'todoList' produce output, else they produce nothing.
todo_include_todos = True
