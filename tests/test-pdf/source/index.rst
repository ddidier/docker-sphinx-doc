NDD Docker Sphinx Tests
=======================

.. toctree::
   :maxdepth: 2

   Markdown document <some-markdown-document.md>



recommonmark
------------

https://pypi.org/project/recommonmark/

See the document in the sidebar.



sphinx.ext.graphviz
-------------------

http://www.sphinx-doc.org/en/master/usage/extensions/graphviz.html

.. graphviz::

   digraph foo {
      "bar" -> "baz" -> "quux";
   }



sphinx.ext.ifconfig
-------------------

http://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html

The next line must be "OK":

.. ifconfig:: release in ('alpha', 'beta', 'rc')

   KO

.. ifconfig:: release in ('0.1.0', '1.0.0', '2.0.0')

   OK



sphinx.ext.imgmath OR sphinx.ext.mathjax
----------------------------------------

http://www.sphinx-doc.org/en/master/usage/extensions/math.html#module-sphinx.ext.mathjax
http://www.sphinx-doc.org/en/master/usage/extensions/math.html#module-sphinx.ext.imgmath

See https://github.com/sphinx-doc/sphinx/issues/2837

Since Pythagoras, we know that :math:`a^2 + b^2 = c^2`.

:math:`\underline{x}=[  x_{1}, ...,  x_{n}]^{T}`



sphinx.ext.todo
---------------

.. todo::

   Fix this thing!



sphinx.copybutton
-----------------

.. code-block:: ruby

  def copy()
    return 'copy'
  end



sphinx.prompt
-------------

.. prompt:: bash

    cd <folder>
    cp <src> \
         <dst>
    cd -



sphinxemoji
-----------

Does not work with PDF generation!



sphinxcontrib.excel_table
-------------------------

https://pypi.org/project/sphinxcontrib-excel-table/

.. excel-table::
   :file: ./some-excel-document.xlsx



sphinxcontrib.mermaid
---------------------

https://github.com/mgaitan/sphinxcontrib-mermaid

.. mermaid::

   sequenceDiagram
      participant Alice
      participant Bob
      Alice->John: Hello John, how are you?
      loop Healthcheck
          John->John: Fight against hypochondria
      end
      Note right of John: Rational thoughts <br/>prevail...
      John-->Alice: Great!
      John->Bob: How about you?
      Bob-->John: Jolly good!



sphinxcontrib.plantuml
----------------------

http://en.plantuml.com/

.. uml::

   @startuml

   user -> (use PlantUML)

   note left of user
      Hello!
   end note

   @enduml

.. uml::

      @startuml

      'style options
      skinparam monochrome true
      skinparam circledCharacterRadius 0
      skinparam circledCharacterFontSize 0
      skinparam classAttributeIconSize 0
      hide empty members

      class Car

      Driver - Car : drives >
      Car *- Wheel : have 4 >
      Car -- Person : < owns

      @enduml

.. uml::

      @startuml

      salt
      {
        Just plain text
        [This is my button]
        ()  Unchecked radio
        (X) Checked radio
        []  Unchecked box
        [X] Checked box
        "Enter text here   "
        ^This is a droplist^
      }

      @enduml
