NDD Docker Sphinx Tests
=======================

.. toctree::
   :maxdepth: 2

   Markdown document <some-markdown-document.md>



recommonmark
------------

https://pypi.org/project/recommonmark/

See the document in the sidebar.



sphinx.ext.extlinks
-------------------

https://www.sphinx-doc.org/en/master/usage/extensions/extlinks.html

Look at :issue:`this issue <123>`.



sphinx.ext.graphviz
-------------------

http://www.sphinx-doc.org/en/master/usage/extensions/graphviz.html

.. graphviz::

   digraph foo {
      "bar" -> "baz" -> "quux";
   }



sphinx.ext.ifconfig
-------------------

http://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html

The next line must be "OK":

.. ifconfig:: release in ('alpha', 'beta', 'rc')

   KO

.. ifconfig:: release in ('0.1.0', '1.0.0', '2.0.0')

   OK



sphinx.ext.imgmath OR sphinx.ext.mathjax
----------------------------------------

http://www.sphinx-doc.org/en/master/usage/extensions/math.html#module-sphinx.ext.mathjax
http://www.sphinx-doc.org/en/master/usage/extensions/math.html#module-sphinx.ext.imgmath

See https://github.com/sphinx-doc/sphinx/issues/2837

Since Pythagoras, we know that :math:`a^2 + b^2 = c^2`.

:math:`\underline{x}=[  x_{1}, ...,  x_{n}]^{T}`



sphinx.ext.todo
---------------

.. todo::

   Fix this thing!



sphinx.copybutton
-----------------

.. code-block:: ruby

  def copy()
    return 'copy'
  end



sphinx.prompt
-------------

.. prompt:: bash

    cd <folder>
    cp <src> \
         <dst>
    cd -



sphinxemoji
-----------

This text includes a smiley face |:smile:| and a snake too! |:snake:|

Don't you love it? |:heart_eyes:|



sphinxcontrib.excel_table
-------------------------

https://pypi.org/project/sphinxcontrib-excel-table/

.. excel-table::
   :file: ./some-excel-document.xlsx



sphinxcontrib.mermaid
---------------------

https://github.com/mgaitan/sphinxcontrib-mermaid

.. mermaid::

   sequenceDiagram
      participant Alice
      participant Bob
      Alice->John: Hello John, how are you?
      loop Healthcheck
          John->John: Fight against hypochondria
      end
      Note right of John: Rational thoughts <br/>prevail...
      John-->Alice: Great!
      John->Bob: How about you?
      Bob-->John: Jolly good!



sphinxcontrib.plantuml
----------------------

http://en.plantuml.com/

.. uml::

   @startuml

   user -> (use PlantUML)

   note left of user
      Hello!
   end note

   @enduml

.. uml::

      @startuml

      'style options
      skinparam monochrome true
      skinparam circledCharacterRadius 0
      skinparam circledCharacterFontSize 0
      skinparam classAttributeIconSize 0
      hide empty members

      class Car

      Driver - Car : drives >
      Car *- Wheel : have 4 >
      Car -- Person : < owns

      @enduml

.. uml::

      @startuml

      salt
      {
        Just plain text
        [This is my button]
        ()  Unchecked radio
        (X) Checked radio
        []  Unchecked box
        [X] Checked box
        "Enter text here   "
        ^This is a droplist^
      }

      @enduml



Custom CSS stylesheet
---------------------

Figure legend
~~~~~~~~~~~~~

Some text before the figure.

.. figure:: /_static/images/external-link.svg
   :width: 200px

   I am legend!

Some text after the figure.

Table alignments
~~~~~~~~~~~~~~~~

**Align left:**

.. rst-class:: "column-1-text-align-left column-2-text-align-left column-3-text-align-left column-4-text-align-left column-5-text-align-left"

+----------------+----------------+----------------+----------------+----------------+
| Large Header 1 | Large Header 2 | Large Header 3 | Large Header 4 | Large Header 5 |
+================+================+================+================+================+
| Value 1 1      | Value 1 2      | Value 1 3      | Value 1 4      | Value 1 5      |
+----------------+----------------+----------------+----------------+----------------+
| Value 2 1      | Valeu 2 2      | Value 2 3      | Value 2 4      | Value 2 5      |
+----------------+----------------+----------------+----------------+----------------+
| Value 3 1      | Value 3 2      | Value 3 3      | Value 3 4      | Value 3 5      |
+----------------+----------------+----------------+----------------+----------------+

**Align center:**

.. rst-class:: "column-1-text-align-center column-2-text-align-center column-3-text-align-center column-4-text-align-center column-5-text-align-center"

+----------------+----------------+----------------+----------------+----------------+
| Large Header 1 | Large Header 2 | Large Header 3 | Large Header 4 | Large Header 5 |
+================+================+================+================+================+
| Value 1 1      | Value 1 2      | Value 1 3      | Value 1 4      | Value 1 5      |
+----------------+----------------+----------------+----------------+----------------+
| Value 2 1      | Value 2 2      | Value 2 3      | Value 2 4      | Value 2 5      |
+----------------+----------------+----------------+----------------+----------------+
| Value 3 1      | Value 3 2      | Value 3 3      | Value 3 4      | Value 3 5      |
+----------------+----------------+----------------+----------------+----------------+

**Align right:**

.. rst-class:: "column-1-text-align-right column-2-text-align-right column-3-text-align-right column-4-text-align-right column-5-text-align-right"

+----------------+----------------+----------------+----------------+----------------+
| Large Header 1 | Large Header 2 | Large Header 3 | Large Header 4 | Large Header 5 |
+================+================+================+================+================+
| Value 1 1      | Value 1 2      | Value 1 3      | Value 1 4      | Value 1 5      |
+----------------+----------------+----------------+----------------+----------------+
| Value 2 1      | Value 2 2      | Value 2 3      | Value 2 4      | Value 2 5      |
+----------------+----------------+----------------+----------------+----------------+
| Value 3 1      | Value 3 2      | Value 3 3      | Value 3 4      | Value 3 5      |
+----------------+----------------+----------------+----------------+----------------+



Custom JS scripts
-----------------

addIconToExternalLinks
~~~~~~~~~~~~~~~~~~~~~~

Some `external link HTTP <http://www.example.com>`_.

Some `external link HTTPS <https://www.example.com>`_.

Some `relative link <some-markdown-document.html>`_.
