# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'MYPROJECT'
copyright = '2023, MYSELF'
author = 'MYSELF'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = []

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']






# == NDD DOCKER SPHINX - OVERRIDE ============================================


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom ones.
extensions = [
    'recommonmark',

    'sphinx.ext.extlinks',
    'sphinx.ext.graphviz',
    'sphinx.ext.ifconfig',
  # 'sphinx.ext.imgmath',
    'sphinx.ext.mathjax',
    'sphinx.ext.todo',

    'sphinx_copybutton',
    'sphinx-prompt',
    'sphinxemoji.sphinxemoji',

    'sphinxcontrib.excel_table',
    'sphinxcontrib.git_context',
  # 'sphinxcontrib.mermaid',
    'sphinxcontrib.plantuml',
]

# If true, 'todo' and 'todoList' produce output, else they produce nothing.
todo_include_todos = True


# -- HTML --------------------------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'sphinx_rtd_theme'

# Must be defined somewhere
html_context = {}

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
#
# html_logo = '_static/logo.png'

# The name of an image file (relative to this directory) to use as a favicon of
# the docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
#
# html_favicon = '_static/favicon.ico'

# If true, links to the reST sources are added to the pages.
#
html_show_sourcelink = False

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#
html_show_sphinx = False

# A custom CSS stylesheet.
#
html_css_files = ['stylesheets/custom.css']

# A custom JS script.
#
html_js_files = ['javascripts/custom.js']

# Provides aliases to base URL.
#
# extlinks = {
#   'issue': ('https://github.com/sphinx-doc/sphinx/issues/%s', None)
# }
