#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Build the HTML and PDF Sphinx documentation.
#
# Examples:
#   ./build-pdf.sh
#   ./build-pdf.sh --name my-documentation
# ------------------------------------------------------------------------------

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

"${PROJECT_DIR}/bin/build.sh" --pdf "${@}"

exit 0
