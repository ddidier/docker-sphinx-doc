#!/usr/bin/env bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/shflags/shflags"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-utils4b/ndd-utils4b.sh"

# disable before shflags
ndd::base::catch_more_errors_off

DEFINE_boolean  "meld"   false  "Open Meld on comparison failures"  "m"
DEFINE_boolean  "debug"  false  "Enable debug mode"                 "d"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on



function main() {

    if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
        ndd::logger::set_stdout_level "DEBUG"
    else
        ndd::logger::set_stdout_level "INFO"
    fi

    log info "Testing the project creation"

    # ---------- create temporary directories

    local temp_dir
    local temp_dir_link
    temp_dir=$(mktemp -d --suffix=.docker-sphinx-doc)
    temp_dir_link="$(dirname "${temp_dir}")/docker-sphinx-doc"

    rm -rf "${temp_dir_link}"
    ln -sf "${temp_dir}" "${temp_dir_link}"

    log info "Test data will be stored in the temporary directory: ${temp_dir}"
    log info "Use the following link for convenience: ${temp_dir_link}"

    # ----------

    # $UID and $USER are not defined in Alpine image
    # local container_uid="${UID}"
    # local container_username="${USER}"
    local container_uid
    container_uid="$(id -u)"

    # Tag the Docker image for testing
    log info "Tagging image 'ddidier/sphinx-doc:latest' with 'ddidier/sphinx-doc:testing'"
    docker tag "ddidier/sphinx-doc:latest" "ddidier/sphinx-doc:testing"

    # Generate a new project
    log info "Generating a new Sphinx project"
    log debug "$(ndd::print::script_output_start)"
    while read -r line; do
        log debug "${line}"
    done < <(
        docker run --rm -v "${temp_dir}":/data -e USER_ID="${container_uid}" ddidier/sphinx-doc:testing \
            sphinx-quickstart-custom --project=MYPROJECT --author=MYSELF --sep --quiet 2>&1
    )
    log debug "$(ndd::print::script_output_end)"

    # Retrieve Git metadata from the Docker image
    local docker_image_sha
    local docker_image_tag
    docker_image_sha="$(docker run --rm -e USER_ID="${container_uid}" "ddidier/sphinx-doc:testing" bash -c 'echo "${GIT_COMMIT_SHA}"')"
    docker_image_tag="$(docker run --rm -e USER_ID="${container_uid}" "ddidier/sphinx-doc:testing" bash -c 'echo "${GIT_COMMIT_TAG}"')"
    log debug "Docker image Git commit = ${docker_image_sha}"
    log debug "Docker image Git tag    = ${docker_image_tag}"

    # Check that metadata has been set in "variables.sh"
    log info "Checking Docker image version in the generated files"

    if ! grep "DDIDIER_SPHINX_IMAGE_VERSION=\"${docker_image_tag}\"" "${temp_dir}/bin/variables.sh" > /dev/null; then
        log error "Wrong or missing Git tag in '${temp_dir}/bin/variables.sh'"
        log error "Expected 'DDIDIER_SPHINX_IMAGE_VERSION=\"${docker_image_tag}\"'"
        log error "Project creation test: FAILED"
        exit 1
    fi

    # Normalize image version for files comparison
    sed -i -E "s/(DDIDIER_SPHINX_IMAGE_VERSION)=.+/\1=\"testing\"/g" "${temp_dir}/bin/variables.sh"

    # Check that metadata has been set in "variables.sh"
    log info "Checking Git metadata in the generated files"

    if ! grep "Docker image commit: ${docker_image_sha}" "${temp_dir}/bin/variables.sh" > /dev/null; then
        log error "Wrong or missing Git commit hash in '${temp_dir}/bin/variables.sh'"
        log error "Expected 'Docker image commit: ${docker_image_sha}'"
        log error "Project creation test: FAILED"
        exit 1
    fi

    if ! grep "Docker image tag: ${docker_image_tag}" "${temp_dir}/bin/variables.sh" > /dev/null; then
        log error "Wrong or missing Git tag in '${temp_dir}/bin/variables.sh'"
        log error "Expected 'Docker image tag: ${docker_image_tag}'"
        log error "Project creation test: FAILED"
        exit 1
    fi

    # Normalize Git metadata for files comparison
    sed -i -E "s/(Docker image commit:).+/\1 TESTED_IN_SCRIPT/g" "${temp_dir}/bin/variables.sh"
    sed -i -E "s/(Docker image tag:).+/\1 TESTED_IN_SCRIPT/g"    "${temp_dir}/bin/variables.sh"

    # ----------

    log info "Committing changes for sphinxcontrib-git_context"
    git -C "$temp_dir" config user.email "test@example.com"
    git -C "$temp_dir" config user.name "Test"
    git -C "$temp_dir" config commit.gpgsign false
    git -C "$temp_dir" add --all > /dev/null
    git -C "$temp_dir" commit -m "Change Docker image version" > /dev/null

    # ----------

    log info "Generating HTML documentation"
    log debug "$(ndd::print::script_output_start)"
    while read -r line; do
        log debug "${line}"
    done < <( "${temp_dir}/bin/build-html.sh" )
    log debug "$(ndd::print::script_output_end)"

    # ----------

    log info "Checking the generated Git commits"

    local actual_git_log_messages
    local expected_git_log_messages

    actual_git_log_messages="$(git -C "${temp_dir}" log --pretty=%s | tr '\n' '@')"
    expected_git_log_messages="Change Docker image version@Generate skeleton@Initial commit@"

    if [[ "${actual_git_log_messages}" != "${expected_git_log_messages}" ]];then
        log error "Wrong or missing generated commits"
        log error "Expected: '${expected_git_log_messages}'"
        log error "Actual:   '${actual_git_log_messages}'"
        log error "Project creation test: FAILED"
        exit 1
    fi

    # ----------

    log info "Checking differences between generated and expected files"

    local expected_dir="${PROJECT_DIR}/tests/test-new-project/expected"

    log debug "Renaming .gitignore"
    mv "${temp_dir}/.gitignore" "${temp_dir}/.gitignore.expected"

    log debug "Getting rid of the generation date"
    sed -i -E 's/(sphinx-quickstart on) .+/\1 DATE_REMOVED./g' "${temp_dir}/source/index.rst"
    sed -i -E 's/(sphinx-quickstart on) .+/\1 DATE_REMOVED./g' "${temp_dir}/build/html/_sources/index.rst.txt"

    log debug "Getting rid of the Git metadata"
    find "${temp_dir}/build" -type f -exec \
        sed -i -E 's/Commit: [0-9a-f]{40}/Commit: FULL_COMMIT_HASH/g' {} \;
    find "${temp_dir}/build" -type f -exec \
        sed -i -E 's/Commit: [0-9a-f]{8}/Commit: SHORT_COMMIT_HASH/g' {} \;
    find "${temp_dir}/build" -type f -exec \
        sed -i -E 's/Last updated on [[:digit:]]{4}\/[[:digit:]]{2}\/[[:digit:]]{2} [[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2}/Last updated on LAST_UPDATED/g' {} \;
    find "${temp_dir}/build" -type f -exec \
        sed -i -E 's/config: [0-9a-f]{32}/config: CONFIG_HASH/g' {} \;

    log debug "Getting rid of javascript hashes"
    find "${temp_dir}/build" -type f -exec \
        sed -i -E 's/\?v=[0-9a-f]{8}/?v=JS_HASH/g' {} \;

    log info "Computing differences between actual and expected files"

    local delta
    delta=$(diff -r --exclude ".git" --exclude "doctrees" --exclude "__pycache__" "${temp_dir}" "${expected_dir}" || true)

    if [[ "${delta}" != "" ]]; then
        log error "Found differences between generated and expected files"
        log debug "$(ndd::print::script_output_start)"
        log debug "${delta}"
        log debug "$(ndd::print::script_output_end)"
        log error "Some useful commands:"
        log error "  diff -r --exclude '.git' --exclude 'doctrees' --exclude '__pycache__' '${temp_dir_link}' '${expected_dir}'"
        log error "  meld '${temp_dir_link}' '${expected_dir}' &"
        log error "Project creation test: FAILED"

        # shellcheck disable=SC2154
        if [[ "${FLAGS_meld}" -eq "${FLAGS_TRUE}" ]] && command -v meld; then
            meld "${temp_dir_link}" "${expected_dir}" &
        fi

        exit 1
    fi

    # ----------

    log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log info "┃ Project creation test: SUCCESS"
    log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    log error "An unexpected error has occured:\n%s" "$(ndd::base::print_stack_trace 2>&1)"

    exit 1
}

trap 'error_handler ${?}' ERR

main "${@}"

exit 0
