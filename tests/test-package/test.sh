#!/usr/bin/env bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/shflags/shflags"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-utils4b/ndd-utils4b.sh"

# disable before shflags
ndd::base::catch_more_errors_off

# shellcheck disable=SC2034
DEFINE_boolean  "meld"   false  "Open Meld on comparison failures"  "m"
DEFINE_boolean  "debug"  false  "Enable debug mode"                 "d"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on



function main() {

    if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
        ndd::logger::set_stdout_level "DEBUG"
    else
        ndd::logger::set_stdout_level "INFO"
    fi

    log info "Testing packaging"

    # ---------- create temporary directories

    local temp_dir
    local temp_dir_link
    local temp_dir_basename
    temp_dir=$(mktemp -d --suffix=.docker-sphinx-doc)
    temp_dir_link="$(dirname "${temp_dir}")/docker-sphinx-doc"
    temp_dir_basename="$(basename "${temp_dir}")"

    rm -rf "${temp_dir_link}"
    ln -sf "${temp_dir}" "${temp_dir_link}"

    log info "Test data will be stored in the temporary directory: ${temp_dir}"
    log info "Use the following link for convenience: ${temp_dir_link}"

    # ----------

    log info "Copying default project files"
    cp -r "${PROJECT_DIR}"/tests/test-new-project/expected/* "${temp_dir}/"

    # ----------

    log info "Packaging documentation with custom name and custom version"

    if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
        log debug "$(ndd::print::script_output_start)"
        "${temp_dir}/bin/package.sh" --name "my-documentation" --version "0.1.2"
        log debug "$(ndd::print::script_output_end)"
    fi

    log info "Checking packaging"
    actual_archive_file_path="${temp_dir}/dist/my-documentation-0.1.2.tar.gz"

    if [[ ! -f "${actual_archive_file_path}" ]]; then
        log error "Missing archive file: ${actual_archive_file_path}"
        exit 1
    fi

    tar xfz "${actual_archive_file_path}" --directory "${temp_dir}/dist"

    local actual_dir="${temp_dir}/dist/my-documentation-0.1.2"
    local expected_dir="${PROJECT_DIR}/tests/test-new-project/expected/build/html"
    local delta
    delta=$(diff -r "${actual_dir}" "${expected_dir}" || true)

    if [[ "${delta}" != "" ]]; then
        log error "Found differences between packaged and expected files"
        log debug "$(ndd::print::script_output_start)"
        log debug "${delta}"
        log debug "$(ndd::print::script_output_end)"
        log error "Some useful commands:"
        log error "  diff -r --exclude '.git' '${actual_dir}' '${expected_dir}'"
        log error "  meld '${actual_dir}' '${expected_dir}' &"
        log error "Packaging test: FAILED"

        # shellcheck disable=SC2154
        if [[ "${FLAGS_meld}" -eq "${FLAGS_TRUE}" ]] && command -v meld; then
            meld "${actual_dir}" "${expected_dir}" &
        fi

        exit 1
    fi

    # ----------

    log info "Packaging documentation with default name and custom version"

    if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
        log debug "$(ndd::print::script_output_start)"
        "${temp_dir}/bin/package.sh" --version "0.1.2"
        log debug "$(ndd::print::script_output_end)"
    fi

    log info "Checking packaging"
    actual_archive_file_path="${temp_dir}/dist/${temp_dir_basename}-0.1.2.tar.gz"

    if [[ ! -f "${actual_archive_file_path}" ]]; then
        log error "Missing archive file: ${actual_archive_file_path}"
        exit 1
    fi

    tar xfz "${actual_archive_file_path}" --directory "${temp_dir}/dist"

    local actual_dir="${temp_dir}/dist/${temp_dir_basename}-0.1.2"
    local expected_dir="${PROJECT_DIR}/tests/test-new-project/expected/build/html"
    local delta
    delta=$(diff -r "${actual_dir}" "${expected_dir}" || true)

    if [[ "${delta}" != "" ]]; then
        log error "Found differences between packaged and expected files"
        log debug "$(ndd::print::script_output_start)"
        log debug "${delta}"
        log debug "$(ndd::print::script_output_end)"
        log error "Some useful commands:"
        log error "  diff -r --exclude '.git' '${actual_dir}' '${expected_dir}'"
        log error "  meld '${actual_dir}' '${expected_dir}' &"
        log error "Packaging test: FAILED"

        # shellcheck disable=SC2154
        if [[ "${FLAGS_meld}" -eq "${FLAGS_TRUE}" ]] && command -v meld; then
            meld "${actual_dir}" "${expected_dir}" &
        fi

        exit 1
    fi

    # ----------

    log info "Packaging documentation with default values"

    if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
        log debug "$(ndd::print::script_output_start)"
        "${temp_dir}/bin/package.sh"
        log debug "$(ndd::print::script_output_end)"
    fi

    log info "Checking packaging"
    actual_archive_file_path="${temp_dir}/dist/${temp_dir_basename}-snapshot.tar.gz"

    if [[ ! -f "${actual_archive_file_path}" ]]; then
        log error "Missing archive file: ${actual_archive_file_path}"
        exit 1
    fi

    tar xfz "${actual_archive_file_path}" --directory "${temp_dir}/dist"

    local actual_dir="${temp_dir}/dist/${temp_dir_basename}-snapshot"
    local expected_dir="${PROJECT_DIR}/tests/test-new-project/expected/build/html"
    local delta
    delta=$(diff -r "${actual_dir}" "${expected_dir}" || true)

    if [[ "${delta}" != "" ]]; then
        log error "Found differences between packaged and expected files"
        log debug "$(ndd::print::script_output_start)"
        log debug "${delta}"
        log debug "$(ndd::print::script_output_end)"
        log error "Some useful commands:"
        log error "  diff -r --exclude '.git' '${actual_dir}' '${expected_dir}'"
        log error "  meld '${actual_dir}' '${expected_dir}' &"
        log error "Packaging test: FAILED"

        # shellcheck disable=SC2154
        if [[ "${FLAGS_meld}" -eq "${FLAGS_TRUE}" ]] && command -v meld; then
            meld "${actual_dir}" "${expected_dir}" &
        fi

        exit 1
    fi

    # ----------

    log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log info "┃ Packaging test: SUCCESS"
    log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    log error "An unexpected error has occured:\n%s" "$(ndd::base::print_stack_trace 2>&1)"

    exit 1
}

trap 'error_handler ${?}' ERR

main "${@}"

exit 0
