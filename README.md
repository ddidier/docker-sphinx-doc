# NDD Docker Sphinx

[![pipeline status](https://gitlab.com/ddidier/docker-sphinx-doc/badges/master/pipeline.svg)](https://gitlab.com/ddidier/docker-sphinx-doc/commits/master)

<!-- MarkdownTOC -->

- [NDD Docker Sphinx](#ndd-docker-sphinx)
  - [Introduction](#introduction)
  - [Installation](#installation)
    - [From source](#from-source)
    - [From Docker Hub](#from-docker-hub)
  - [Usage](#usage)
    - [Initialisation](#initialisation)
    - [Non interactive mode](#non-interactive-mode)
      - [Helper scripts](#helper-scripts)
      - [Docker commands](#docker-commands)
    - [Interactive mode](#interactive-mode)
    - [Tips \& Tricks](#tips--tricks)
  - [Configuration](#configuration)
    - [Bundled extensions](#bundled-extensions)
    - [Add new extensions](#add-new-extensions)
  - [Limitations](#limitations)
  - [Development](#development)
    - [Coding](#coding)
    - [Testing locally with gitlab-runner](#testing-locally-with-gitlab-runner)
    - [Releasing](#releasing)

<!-- /MarkdownTOC -->



<a id="introduction"></a>
## Introduction

[Sphinx] documentation builder with support for HTML, live HTML and PDF generation, and a lot more...

The versioning scheme of this Docker image is `<SPHINX_VERSION>-<DOCKER_IMAGE_VERSION>`.
For example, `4.1.2-1` stands for the first version of this Docker image using Sphinx `4.1.2`.

Besides the official [Sphinx] documentation builder, this image contains and is configured for the following extensions:

- [guzzle-sphinx-theme]
- [livereload]
- [recommonmark]
- [rinohtype]
- [sphinx-autobuild]
- [sphinx-bootstrap-theme]
- [sphinx-copybutton]
- [sphinx-prompt]
- [sphinx-rtd-theme]
- [sphinxcontrib-actdiag] REMOVED in 7.1.2 (unmaintained)
- [sphinxcontrib-blockdiag] REMOVED in 7.1.2 (unmaintained)
- [sphinxcontrib-excel-table]
- [sphinxcontrib-fulltoc]
- [sphinxcontrib-git_context]
- [sphinxcontrib-mermaid]
- [sphinxcontrib-nwdiag] REMOVED in 7.1.2 (unmaintained)
- [sphinxcontrib-plantuml]
- [sphinxcontrib-seqdiag] REMOVED in 7.1.2 (unmaintained)
- [sphinxemoji]

This image also contains some utility scripts to make your life easier.

References:

- image on Docker Hub : https://hub.docker.com/r/ddidier/sphinx-doc
- sources on GitLab : https://gitlab.com/ddidier/docker-sphinx-doc

The image is based on the official [python:3.8](https://hub.docker.com/_/python/).



<a id="installation"></a>
## Installation

<a id="from-source"></a>
### From source

The sources are available on [GitLab](https://gitlab.com/ddidier/sphinx-doc).

```shell
git clone git@gitlab.com:ddidier/docker-sphinx-doc.git
cd docker-sphinx-doc
make
```

<a id="from-docker-hub"></a>
### From Docker Hub

The Docker image is available on [Docker Hub](https://hub.docker.com/r/ddidier/sphinx-doc)

```shell
export SPHINX_DOC_VERSION="4.1.2-1"
docker pull ddidier/sphinx-doc:"${SPHINX_DOC_VERSION}"
```



<a id="usage"></a>
## Usage

The documentation directory on the host is called `${HOST_DATA_DIR}` in the remainder of this document.

The directory `${HOST_DATA_DIR}` on the host must be mounted as a volume under `/data` in the container.
Use `-v "${HOST_DATA_DIR}":/data` to use a specific documentation directory.
Use `-v "${PWD}":/data` to use the current directory as the documentation directory.

[Sphinx] is executed inside the container by a custom user who is created by the custom Docker entry point.
You **must** pass to the container the environment variable `USER_ID` set to the UID of the user the files will belong to.
This is the ``-e USER_ID="${UID}" `` part in the examples of this documentation.

ℹ️ **Helper scripts are provided which already take care of this plumbing.**

<a id="initialisation"></a>
### Initialisation

[Sphinx] provides the `sphinx-quickstart` command to create the skeleton of a project.
You should however use the custom tailored `sphinx-quickstart-custom` command.
This command customizes the generated configuration files and adds the helper scripts.

⚠️ **The directory `${HOST_DATA_DIR}` must already exist, otherwise the script will fail!**

```shell
docker run -it --rm -v "${HOST_DATA_DIR}":/data -e USER_ID="${UID}" ddidier/sphinx-doc:"${SPHINX_DOC_VERSION}" sphinx-quickstart-custom
```

The `sphinx-quickstart-custom` command first calls the original `sphinx-quickstart` command with the provided arguments.
Therefore all arguments accepted by [`sphinx-quickstart`](https://www.sphinx-doc.org/en/master/man/sphinx-quickstart.html) are valid.
For example, you may want to pass the project name on the command line:

```shell
docker run -it -v "${HOST_DATA_DIR}":/data -e USER_ID="${UID}" ddidier/sphinx-doc:"${SPHINX_DOC_VERSION}" sphinx-quickstart-custom --project my-documentation
```

You should now customize the content of `bin/variables.sh`, mostly the project name and the exposed port.

<a id="non-interactive-mode"></a>
### Non interactive mode

The so-called *non-interactive mode* is when you issue commands directly from the host.

<a id="helper-scripts"></a>
#### Helper scripts

ℹ️ **Helper scripts are provided to help you with common tasks and are the recommanded way to go.**

They are located in the `bin` directory of the generated project.
They are configured with default variables located in the `bin/variables.sh` file.
Each one of them can be called with `--help` to display all the available options.

To generate the HTML documentation, call:

```shell
./bin/build-html.sh
```

To generate the HTML documentation, and watch for changes with live reload, call:

```shell
# use the default port (i.e. 8000)
./bin/serve.sh

# use a custom port (e.g. 12345) so you can have multiple builds at the same time
./bin/serve.sh --port 12345

# use --help to see all the available options
./bin/serve.sh --help
```

To generate the PDF documentation, call:

```shell
./bin/build-pdf.sh
```

ℹ️ **And to make your life even a little easier, a `Makefile` is also generated at the root of your project.**

Usage is really simple at the expanse of not being customizable:

- to generate the HTML documentation, just call `make html`
- to generate the HTML and the PDF documentations, just call `make pdf`
- to generate and serve the HTML documentation, just call `make serve`
- to package the documentation, just call `make package`

<a id="docker-commands"></a>
#### Docker commands

You can of course directly use the [Sphinx] commands without relying on the helper scripts.

To see all the official targets, call:

```shell
docker run -it --rm -v "${HOST_DATA_DIR}":/data -e USER_ID="${UID}" ddidier/sphinx-doc:"${SPHINX_DOC_VERSION}" make help
```

To generate the HTML documentation, call `make html`:

```shell
docker run -it --rm -v "${HOST_DATA_DIR}":/data -e USER_ID="${UID}" ddidier/sphinx-doc:"${SPHINX_DOC_VERSION}" make html
```

To generate the HTML documentation, and watch for changes with live reload, call `make livehtml`:

```shell
# use the default port (i.e. 8000)
docker run -it --rm -v "${HOST_DATA_DIR}":/data -p 8000:8000 -e USER_ID="${UID}" ddidier/sphinx-doc:"${SPHINX_DOC_VERSION}" make livehtml
#                                               ^^^^^^^^^^^^
#                                               open your browser at http://localhost:8000/

# use a custom port (e.g. 12345) so you can have multiple builds at the same time
docker run -it --rm -v "${HOST_DATA_DIR}":/data -p 12345:12345 -e USER_ID="${UID}" ddidier/sphinx-doc "${SPHINX_DOC_VERSION}" SPHINXPORT=12345 make livehtml
#                                               ^^^^^^^^^^^^^^                                                                ^^^^^^^^^^^^^^^^
#                                               open your browser at http://localhost:12345/                                  customize server port
```

To trigger a full build while in watch mode, issue from the `${HOST_DATA_DIR}` folder on the host:

```shell
rm -rf build && touch source/conf.py
```

To generate the PDF documentation, call `make latexpdf`:

```shell
docker run -it --rm -v "${HOST_DATA_DIR}":/data -e USER_ID="${UID}" ddidier/sphinx-doc:"${SPHINX_DOC_VERSION}" make latexpdf
```

<a id="interactive-mode"></a>
### Interactive mode

The so-called *interactive mode* is when you issue commands from inside the container.

```shell
docker run -it --rm -v "${HOST_DATA_DIR}":/data -e USER_ID="${UID}" ddidier/sphinx-doc:"${SPHINX_DOC_VERSION}"
```

You should now be in the `/data` directory, otherwise just `cd` to `/data`.

You can now use [Sphinx] in the original way:

- to see all the official targets, call `make help`.
- to generate the HTML documentation, call `make html`
- to generate the PDF documentation, call `make latexpdf`
- to generate and serve the HTML documentation, call `make livehtml`

<a id="tips--tricks"></a>
### Tips & Tricks

If you need the directory `${HOST_DATA_DIR}` to NOT be the root of the documentation, change the `make` directory with `-C`.
Previous commands such as `make target` become `make -C /some/directory/ target`.



<a id="configuration"></a>
## Configuration

⚠️ **To simplify the `sphinx-quickstart-custom` script, some variables are overriden at the end of the `conf.py` file.**

Some variables may then appear twice in the file, so be sure to update the last one if needed.
This is most notably the case of the `extensions` and `html_theme` variables.

<a id="bundled-extensions"></a>
### Bundled extensions

This image comes with a number of already bundled extensions.

In most cases:

- to enable a bundled extension, simply uncomment the associated line in your `conf.py`.
- to disable a bundled extension, simply comment the associated line in your `conf.py`.

<a id="add-new-extensions"></a>
### Add new extensions

If you want to use an extension which is not already bundled with this image, you need to:

1. create a new `Dockerfile` extending the `ddidier/sphinx-doc` image
2. install the Python package(s) of the extension(s) inside the `Dockerfile`
3. reference the extension(s) in the `conf.py` configuration file

```docker
#
# Dockerfile
#
FROM ddidier/sphinx-doc:latest

RUN pip install 'a-sphinx-extension       == A.B.C' \
                'another-sphinx-extension == X.Y.Z'
```

```python
#
# source/conf.py
#
extensions = [
    ...
    'a.sphinx.extension',
    'another.sphinx.extension',
]
```



<a id="limitations"></a>
## Limitations

- PDF generation does not take into account Excel tables.



<a id="development"></a>
## Development

<a id="coding"></a>
### Coding

The `Makefile` provide some useful targets:

- `make setup` to install the development tools
- `make build` to build the Docker image
- `make quality` to run the quality checks ([ShellCheck])
- `make test` to run the tests
- `make` is the default target and is a shortcut to `make build quality test`

⚠️ **Do not forget to rebuild the Docker image before testing!**

<a id="testing-locally-with-gitlab-runner"></a>
### Testing locally with gitlab-runner

GitLab-CI is used to do continuous integration testing.

The GitLab-CI environment has some differences with your local environment, mostly:

- the build is done using Docker in Docker
- the Docker image is based on Alpine

The GitLab-CI pipeline takes some time to complete so tests should be done locally before pushing modifications...

1. Start the GitLab Runner service:

    ```shell
    export GITLAB_RUNNER_VERSION=v12.9.0

    docker run -d \
        --name gitlab-runner \
        --privileged \
        -v /srv/gitlab-runner/config:/etc/gitlab-runner \
        -v /var/run/docker.sock:/var/run/docker.sock \
        gitlab/gitlab-runner:"${GITLAB_RUNNER_VERSION}"
    ```

2. Comment out the Docker image pull in `gitlab-ci.yml` since the test will use your local image:

    ```yaml
    Test:
      stage: test
      script:
        # Comment this out when testing locally with gitlab-runner
        # ----------
        # - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
        # - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $DOCKERHUB_IMAGE_NAME:latest
        # ----------
        - export TMPDIR=${CI_PROJECT_DIR}
        - ${CI_PROJECT_DIR}/tests/test.sh
    ```

3. Change what you need
4. Ensure that all modifications are committed, e.g.

    ```shell
    git add --all
    git commit -m "Test or whatever"
    ```

5. Ensure that all modifications have found their way to the Docker image:

    ```shell
    make build
    ```

6. Run the test:

    ```shell
    docker run --rm -t -i \
        -v /opt/gitlab-runner/config:/etc/gitlab-runner \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v "${PWD}":"${PWD}" \
        --workdir "${PWD}" \
        gitlab/gitlab-runner:"alpine-${GITLAB_RUNNER_VERSION}" \
            exec docker \
                --docker-privileged \
                --docker-pull-policy="if-not-present" \
                --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
                --docker-volumes /builds:/builds \
                Test
    ```

7. Repeat from step #3 as needed

Once you've done:

- delete the build directory with `rm -rf /builds`
- revert the comment you've done in `gitlab-ci.yml`

<a id="releasing"></a>
### Releasing

Docker Hub doesn't provide an API to update the README anymore:
when a release has been created, don't forget to manually update the README on Docker Hub.





[Sphinx]: https://www.sphinx-doc.org/

[alabaster]: https://pypi.python.org/pypi/alabaster
[gitpython]: https://pypi.python.org/pypi/gitpython
[guzzle-sphinx-theme]: https://pypi.python.org/pypi/guzzle_sphinx_theme
[livereload]: https://pypi.python.org/pypi/livereload
[recommonmark]: https://pypi.org/project/recommonmark/
[rinohtype]: https://pypi.org/project/rinohtype/
[sphinx-autobuild]: https://pypi.org/project/sphinx-autobuild
[sphinx-bootstrap-theme]: https://pypi.python.org/pypi/sphinx-bootstrap-theme
[sphinx-copybutton]: https://pypi.org/project/sphinx-copybutton/
[sphinx-prompt]: https://pypi.python.org/pypi/sphinx-prompt
[sphinx-rtd-theme]: https://pypi.python.org/pypi/sphinx_rtd_theme
[sphinxcontrib-actdiag]: https://pypi.python.org/pypi/sphinxcontrib-actdiag
[sphinxcontrib-blockdiag]: https://pypi.python.org/pypi/sphinxcontrib-blockdiag
[sphinxcontrib-excel-table]: https://pypi.python.org/pypi/sphinxcontrib-excel-table
[sphinxcontrib-fulltoc]: https://pypi.org/project/sphinxcontrib-fulltoc
[sphinxcontrib-git_context]: https://pypi.org/project/sphinxcontrib-git-context/
[sphinxcontrib-mermaid]: https://pypi.org/project/sphinxcontrib-mermaid/
[sphinxcontrib-nwdiag]: https://pypi.python.org/pypi/sphinxcontrib-nwdiag
[sphinxcontrib-plantuml]: https://pypi.python.org/pypi/sphinxcontrib-plantuml
[sphinxcontrib-seqdiag]: https://pypi.python.org/pypi/sphinxcontrib-seqdiag
[sphinxemoji]: https://pypi.org/project/sphinxemoji/
