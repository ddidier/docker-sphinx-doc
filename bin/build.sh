#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Build the 'ddidier/sphinx-doc' Docker image.
# ------------------------------------------------------------------------------

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/shflags/shflags"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/files/usr/share/ddidier/ndd-utils4b/ndd-utils4b.sh"

# disable before shflags
ndd::base::catch_more_errors_off

DEFINE_boolean  "debug"  false  "Enable debug mode"  "d"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on



function main() {

    if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
        ndd::logger::set_stdout_level "DEBUG"
    else
        ndd::logger::set_stdout_level "INFO"
    fi

    local git_commit_sha
    local git_commit_tag
    git_commit_sha=$(git log -1 --pretty=%H)
    git_commit_tag=$(git describe --exact-match --tags "${git_commit_sha}" 2> /dev/null || echo "testing")

    log info "Building the Docker image 'ddidier/sphinx-doc' with:"
    log info "- Git commit = ${git_commit_sha}"
    log info "- Git tag    = ${git_commit_tag}"

    log debug "$(ndd::print::script_output_start)"

    while read -r line; do
        log debug "${line}"
    done < <(                                               \
        docker image build                                  \
            --tag "ddidier/sphinx-doc"                      \
            --build-arg GIT_COMMIT_SHA="${git_commit_sha}"  \
            --build-arg GIT_COMMIT_TAG="${git_commit_tag}"  \
            "${PROJECT_DIR}/src/"                           \
    )

    docker image tag "ddidier/sphinx-doc:latest" "ddidier/sphinx-doc:${git_commit_tag}"

    log debug "$(ndd::print::script_output_end)"

    log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log info "┃ The Docker image 'ddidier/sphinx-doc' was successfully built"
    log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    log error "An unexpected error has occured:\n%s" "$(ndd::base::print_stack_trace 2>&1)"
    log error "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log error "┃ TESTS -- Failed!                                                             "
    log error "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

    exit 1
}

trap 'error_handler ${?}' ERR

main "${@}"

exit 0
